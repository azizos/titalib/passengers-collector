
FROM mongo
ARG MONGO_DB_COLLECTION
ARG MONGO_URI
COPY seed-data/ seed-data
CMD mongoimport --uri $MONGO_URI --type csv -c $MONGO_DB_COLLECTION --headerline --drop seed-data/*.csv