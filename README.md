# PASSENGERS-COLLECTOR (for Titalib)
This project is used to build and store/register MongoDB-based images that act as seeders and will be used to persist CSV data into a MongoDB database running on a seperate server. The data concern information about surviving Titanic passengers.

## How to use?
1. Pull the image from the registry (it gets created with this project's pipeline builds)
2. Run the container with all following environment variables below (required)
   
| Env                 | Description     | Example value                 |
| ------------------- | --------------- | ----------------------------- |
| MONGO_URI           | MongoDB URI     | mongodb://mongo:27017/titalib |
| MONGO_DB_COLLECTION | Collection name | passengers                    |

_NOTE: if you wish to change the sample data; simply clone this project, add/remove CSV file(s) located in `seed-data` directory and run the container (step 2 above)_